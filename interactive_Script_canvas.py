#export DISPLAY = 0
import os
import glob
import json 
#from Tkinter import Tk, Label, Button
from Tkinter import *
from PIL import ImageTk, Image
import pandas as pd
import argparse
import pydicom
import numpy as np
import cv2
import clock_angle_overlay
import copy
import math
import tkFont

Size1 = 1600
Size2 = 2048

class POINT:
        def __init__(self,x,y):
                self.x = x
                self.y = y

class MyFirstGUI:
    def __init__(self, master,gt,Dir,pointer):
        self.master = master
        self.gt = gt
        self.Dir = Dir
        self.pointer = pointer
        master.title("INTERACTIVE SCRIPT")
        self.scale = 1
        self.rerender = 0
        self.main_window()

    def main_window(self):
        self.label = Label(self.master, text="This is our first GUI!")     
        self.Frame1 = Frame(self.master, highlightbackground = "green", highlightthickness = 1,width = 800, height = 1000, bd =0)
        self.Frame1.grid(row = 0, column = 0, rowspan = 1000, columnspan = 800, sticky = W+E+N+S) 
        helv36 = tkFont.Font(family='Helvetica', size=24, weight='bold')
        self.greet_button = Button(self.Frame1, text="Render clock angle and GT markings", font = helv36, command=self.overlay_all).grid(row=1,column=1,sticky=E+W)
        #self.close_button = Button(Frame1, text="Close", command=master.quit).grid(row=999,column=2,sticky=E+W)
        self.get_sopuid_clockangel_cmfn()
        
        self.Frame2 = Frame(self.master, highlightbackground = "blue", highlightthickness = 1,width = 800, height = 1000)
        self.Frame2.grid(row = 1001, column = 0, rowspan = 2000, columnspan = 800, sticky = W+E+N+S)
        self.another_button = Button(self.Frame2, text = "Render protractor over the image", font = helv36, command = self.overlay_protractor).grid(row=1001, column=1,sticky = N+E+W+S)
        
        OverlayBothButton = Button(self.Frame2, text = "Overlay Both GT and Protractor", font = helv36, command = self.overlay_protractor_and_GT).grid(row = 1002, column =1, sticky = N+E+W+S )

        self.Frame3 = Frame(self.master, highlightbackground = "yellow", highlightthickness = 1, width = 1600, height = 2000)
        self.Frame3.grid(row = 0, column = 801, rowspan = 2000, columnspan = 2600, sticky = W+E+N+S)
        #self.random_button = Button(Frame3,text = "Button in frame 3").grid(row = 1999, column = 1, sticky = N+E+W+S)
        self.First_Time = True
        self.create_image(self.master,self.gt,self.pointer,self.Frame3)
        OriginalImageButton = Button(self.Frame1, text = "Original Image", font = helv36, command = self.back_to_orginal_image).grid(row = 2, column = 1, sticky = S)
        next_button = Button(self.Frame1, text ="Next", font = helv36, command = self.next_dicom).grid(row = 10, column = 2)
        back_button = Button(self.Frame1, text = "Back", font = helv36,command = self.previous_dicom).grid(row = 11,column = 2)

        SaveButton = Button(self.Frame1, text = "Save", font = helv36, command = self.save).grid(row = 800,column = 1, sticky = S)
        RerenderButton = Button(self.Frame1, text = "Rerender for different nipple location", font = helv36, command = self.Do_you_want_to_rerender).grid(row = 900, column = 1, sticky = S)
        #path = "/mnt/Array/share/users/anirudh/Images/Mass/"
        #os.chdir(path)
    
    def get_sopuid_clockangel_cmfn(self):
        self.sopuid = gt.loc[self.pointer,'sop']
        self.clock_angle = self.gt.loc[self.pointer,'clock_angle']
        self.cmfn   = float(self.gt.loc[self.pointer,'distance'])
        helv18 = tkFont.Font(family='Helvetica', size=18, weight='normal')
        var1 = StringVar()
        var1.set("SOP-UID : {}".format(self.sopuid))
        var2 = StringVar()
        var2.set("Clock Angel : {}".format(self.clock_angle))
        var3 = StringVar()
        var3.set("Distance in cms : {}".format(self.cmfn))
        Label(self.Frame1, textvariable = var1, font = helv18 ).grid(row = 6, column = 1, sticky = W)
        Label(self.Frame1, textvariable = var2, font = helv18).grid(row=7, column = 1, sticky = W )
        Label(self.Frame1, textvariable = var3, font = helv18).grid(row=8, column = 1, sticky = W)
        #return sopuid, clock_angle, cmfn 
    
    def back_to_orginal_image(self):
        self.image = im_overlay.copy()
        n_col = Size2
        n_row = Size1
        self.image = cv2.resize(self.image,(Size1,Size2))
        self.image = Image.fromarray(self.image)
        self.image = ImageTk.PhotoImage(self.image)
        self.show_image(self.Frame3,n_col,n_row)


    def greet(self):
        print "Clock angle and markings rendered!"

    def next_dicom(self):
        length = len(self.gt['sop'])
        if self.First_Time != True:
            #self.Frame3.grid_forget()
            if self.pointer != length:
                self.pointer = self.pointer + 1
            else:
                self.pointer = self.pointer
            self.clear_earlier_image()
            self.create_image(self.master,self.gt,self.pointer,self.Frame3)
            self.get_sopuid_clockangel_cmfn()
            print self.pointer    
        else :
            self.First_Time = False
            print 'why god why'
        #return pointer

    def previous_dicom(self):
        #if self.First_Time == False:
        #    self.First_Time = False
        #    print('previous')
        #else:
        #self.Frame3.grid_forget()
        if self.pointer != 0:
            self.pointer = self.pointer - 1
        else:
            self.pointer = self.pointer
        self.clear_earlier_image()
        self.create_image(self.master,self.gt,self.pointer,self.Frame3)
        self.get_sopuid_clockangel_cmfn()
        #return pointer

    def filenames(self,gt,pointer):
        sopuid = gt.loc[pointer,'sop']
        #print(Dir)
        dcm_filename = Dir + 'DCMs/' + sopuid + '.dcm'
        png_filename1 = Dir + 'outputs/' + sopuid +'._1_.png'
        png_filename2 = Dir + 'outputs/' + sopuid + '_2_.png'
        png_filename3 = Dir + 'outputs/' + sopuid + '_3_.png'
        nipple_filename = Dir + 'JSON-Outputs/' + sopuid + '_MassCalc.json'
        breast_contour_filename = Dir + 'BreastBoundary_Output/' + sopuid + '.json'
        return sopuid,dcm_filename,png_filename1,png_filename2,png_filename3,nipple_filename,breast_contour_filename

    def create_image(self,master,gt,pointer,Frame3):
        sopuid,dcm_filename,png_filename1,png_filename2,png_filename3,nipple_filename,breast_contour_filename = self.filenames(gt,pointer)
        if os.path.isfile(dcm_filename):
            #read DCM
            print 'reading: ',dcm_filename
            dicom_object = pydicom.dcmread(dcm_filename)
            im = np.array((float(255)/float(4096))* np.array(dicom_object.pixel_array[:,:]).astype('float')).astype('uint8')
            #get image size
            image_size = im.shape
            self.n_row      = image_size[0]
            self.n_col      = image_size[1]
            im_resized = cv2.resize(im, (self.n_col,self.n_row))
            #store image in im_overlay (3 channels image overlay array)
            im_overlay = np.zeros((self.n_row,self.n_col,3)).astype('uint8')
            im_overlay[:,:,0] = np.copy(im_resized)
            im_overlay[:,:,1] = np.copy(im_resized)
            im_overlay[:,:,2] = np.copy(im_resized)
            #Converting image into PIL format so that it can be shown in the GUI
            global im_overlay
            self.image_ori = im_overlay.copy()
            image = im_overlay.copy()
            image = cv2.resize(image,(Size1,Size2))
            self.image = image
            self.image = Image.fromarray(self.image)
            self.image = ImageTk.PhotoImage(self.image)
            n_col = Size2
            n_row = Size1
            #self.Frame3 = Frame(self.master, highlightbackground = "yellow", highlightthickness = 1, width = 1600, height = 2000)
            #self.Frame3.grid(row = 0, column = 801, rowspan = 2000, columnspan = 2600, sticky = W+E+N+S)
            #self.panel = Label(Frame3,image = self.image)
            #self.panel.image = self.image
            #self.panel.grid(row = 0, column = 801)
            self.show_image(Frame3,n_col,n_row)

    def show_image(self,Frame3,n_col,n_row):
            Frame3.update_idletasks()
            self.canvas = Canvas(Frame3,height = n_col, width = n_row)
            self.canvas.grid(row=0,column = 0)
            self.item = self.canvas.create_image(0,0,anchor = N+W,image = self.image, tags = "main")
            #self.canvas.scale(ALL,0,0,self.scale,self.scale)
            self.canvas.grid_propagate(False)
            #Scroll bar 
            #self.canvas.focus_set()
            vsb = Scrollbar(self.canvas, orient="vertical", command=self.canvas.yview)
            vsb.grid(row=0)
            self.canvas.configure(yscrollcommand=vsb.set)
            hsb = Scrollbar(self.canvas, orient = "horizontal", command = self.canvas.xview)
            hsb.grid(row =1)
            self.canvas.configure(xscrollcommand=hsb.set)
            self.canvas.config(scrollregion=self.canvas.bbox("all"),confine = False)    
            #self.canvas.tag_raise(self.item2)        
            #Drag and Drop
            self._drag_data = {"x": 0, "y": 0, "item": None}
            self._create_token1((100, 100), "blue")
            self._create_token2((200, 100), "green")
            self._create_token3((300, 100), "red")
            self.canvas.tag_bind(self.item2, "<ButtonPress-1>", self.on_token_press)
            self.canvas.tag_bind(self.item2, "<ButtonRelease-1>", self.on_token_release1)
            self.canvas.tag_bind(self.item2, "<B1-Motion>", self.on_token_motion1)

            self.canvas.tag_bind(self.item4, "<ButtonPress-1>", self.on_token_press)
            self.canvas.tag_bind(self.item4, "<ButtonRelease-1>", self.on_token_release2)
            self.canvas.tag_bind(self.item4, "<B1-Motion>", self.on_token_motion2)

            self.canvas.tag_bind(self.item5, "<ButtonPress-1>", self.on_token_press)
            self.canvas.tag_bind(self.item5, "<ButtonRelease-1>", self.on_token_release3)
            self.canvas.tag_bind(self.item5, "<B1-Motion>", self.on_token_motion3)

            self.item3 = self.canvas.create_text(0,0,fill="Black",font="Times 20 italic bold",
                        text="Marker for GT/Mass Lesion", tags = "text")
            self.item6 = self.canvas.create_text(0,0,fill="Black",font="Times 20 bold",
                        text="Marker for nipple location", tags = "text")
            #self.canvas.tag_bind(self.item,"<Button 4>",self.zoom)
            #self.canvas.tag_bind(self.item,"<Button 5>",self.zoom_out)
            #self.redraw()
            #self.canvas.see('end')
            #self.next_dicom()
            #return im_overlay

    def Do_you_want_to_rerender(self):
        self.rerender = 1

    def _create_token1(self, coord, color):
        '''Create a token at the given coordinate in the given color'''
        x,y = coord
        self.item2 = self.canvas.create_oval(x-50, y-50, x+50, y+50, outline=color, tags="token")       
        
    def _create_token2(self, coord, color):
        x,y = coord
        self.item4 = self.canvas.create_oval(x-50, y-50, x+50, y+50, outline=color, tags="token")

    def _create_token3(self, coord, color):
        x,y = coord
        self.item5 = self.canvas.create_oval(x-50, y-50, x+50, y+50, outline=color, tags="token")
    
    def on_token_press(self, event):
        '''Begining drag of an object'''
        # record the item and its location
        self._drag_data["item"] = self.canvas.find_closest(event.x, event.y)[0]
        self._drag_data["x"] = self.canvas.canvasx(event.x)
        self._drag_data["y"] = self.canvas.canvasy(event.y)

    def on_token_release1(self, event):
        '''End drag of an object'''
        # reset the drag information
        self._drag_data["item"] = None
        self._drag_data["x"] = self.canvas.canvasx(event.x)
        self._drag_data["y"] = self.canvas.canvasy(event.y)
        self.GT_x = (self.canvas.canvasx(event.x)/Size1)*self.n_col
        self.GT_y = (self.canvas.canvasy(event.y)/Size2)*self.n_row
        self.getbackClockAngle_and_distance(self.canvas.canvasx(event.x),self.canvas.canvasy(event.y))

    def on_token_release2(self, event):
        '''End drag of an object'''
        # reset the drag information
        self._drag_data["item"] = None
        self._drag_data["x"] = self.canvas.canvasx(event.x)
        self._drag_data["y"] = self.canvas.canvasy(event.y)
        self.ML_x = (self.canvas.canvasx(event.x)/Size1)*self.n_col
        self.ML_y = (self.canvas.canvasy(event.y)/Size2)*self.n_row
        self.getbackClockAngle_and_distance(self.canvas.canvasx(event.x),self.canvas.canvasy(event.y))

    def on_token_release3(self, event):
        '''End drag of an object'''
        # reset the drag information
        self.canvas.delete(self.item6)
        self._drag_data["item"] = None
        self._drag_data["x"] = self.canvas.canvasx(event.x)
        self._drag_data["y"] = self.canvas.canvasy(event.y)
        self.NL_x = (self.canvas.canvasx(event.x)/Size1)*self.n_col
        self.NL_y = (self.canvas.canvasy(event.y)/Size2)*self.n_row
        var1 = "x: {0}, y: {1}".format(int(self.NL_x),int(self.NL_y))
        self.item6 = self.canvas.create_text(self.canvas.canvasx(event.x),self.canvas.canvasy(event.y),fill="White",font="Times 20 bold",
                        text=var1, tags = "text")
        if self.rerender == 1:
            self.rerender_protractor(self.canvas.canvasx(event.x),self.canvas.canvasy(event.y))
            self.rerender = 0
        #self.getbackClockAngle_and_distance(self.canvas.canvasx(event.x),self.canvas.canvasy(event.y))

    def on_token_motion1(self, event):
        '''Handle dragging of an object'''
        # compute how much the mouse has moved
        delta_x = self.canvas.canvasx(event.x) - self._drag_data["x"]
        delta_y = self.canvas.canvasy(event.y) - self._drag_data["y"]
        # move the object the appropriate amount
        self.canvas.move(self.item2, delta_x, delta_y)
        #self.item2.move(self._drag_data["item"], delta_x, delta_y)
        # record the new position
        self._drag_data["x"] = self.canvas.canvasx(event.x)
        self._drag_data["y"] = self.canvas.canvasy(event.y)

    def on_token_motion2(self, event):
        '''Handle dragging of an object'''
        # compute how much the mouse has moved
        delta_x = self.canvas.canvasx(event.x) - self._drag_data["x"]
        delta_y = self.canvas.canvasy(event.y) - self._drag_data["y"]
        # move the object the appropriate amount
        self.canvas.move(self.item4, delta_x, delta_y)
        #self.item2.move(self._drag_data["item"], delta_x, delta_y)
        # record the new position
        self._drag_data["x"] = self.canvas.canvasx(event.x)
        self._drag_data["y"] = self.canvas.canvasy(event.y)

    def on_token_motion3(self, event):
        '''Handle dragging of an object'''
        # compute how much the mouse has moved
        delta_x = self.canvas.canvasx(event.x) - self._drag_data["x"]
        delta_y = self.canvas.canvasy(event.y) - self._drag_data["y"]
        # move the object the appropriate amount
        self.canvas.move(self.item5, delta_x, delta_y)
        #self.item2.move(self._drag_data["item"], delta_x, delta_y)
        # record the new position
        self._drag_data["x"] = self.canvas.canvasx(event.x)
        self._drag_data["y"] = self.canvas.canvasy(event.y)

    def zoom(self,event):
        if event.num == 4:
            self.scale *= 2
        elif event.num == 5:
            self.scale *= 0.5
        self.zoom_in(event.x,event.y)

    def redraw(self,x,y):
        iw = self.image.width()
        ih = self.image.height()
        self.clear_earlier_image()
        #cw, ch = iw / self.scale, ih / self.scale
        size = int(iw * self.scale), int(ih * self.scale)
        tmp = self.image_ori
        tmp = cv2.resize(tmp,(int(iw * self.scale),int(ih * self.scale)))
        self.image_ori = tmp
        tmp = Image.fromarray(tmp)
        del self.image
        self.image = ImageTk.PhotoImage(tmp)
        self.show_image(self.Frame3,ih,iw)
        
    def zoom_in(self,x,y):
        self.clear_earlier_image()
        #crop the image 
        x = self.canvas.canvasx(x)
        y = self.canvas.canvasy(y)
        tmp = self.image_ori
        new_image = tmp[1:1600-1,int(y)-1:2048-1]
        new_image = cv2.resize(new_image,(1600,2048))
        self.image_ori = new_image
        del self.image
        new_image = Image.fromarray(new_image)
        self.image = ImageTk.PhotoImage(new_image)
        self.show_image(self.Frame3,2048,1600)

    def zoom_out(self):
        image = self.image_ori
        image = Image.fromarray(image)
        self.image = ImageTk.PhotoImage(image)
        self.show_image(self.Frame3,2048,1600)


    def clear_earlier_image(self):
            self.canvas.delete(self.item)
            #self.canvas.delete(self.item2)

    def get_values(self):
        sopuid,dcm_filename,png_filename1,png_filename2,png_filename3,nipple_filename,breast_contour_filename = self.filenames(self.gt,self.pointer)
        if os.path.isfile(nipple_filename):
            #get nipple's bounding box, centroid coordinates, and pertinent information regarding DCM image
            image_laterality,view_position,pixel_size,xmin,ymin,xmax,ymax,nipple = clock_angle_overlay.get_nipple(nipple_filename)
            #overlay nipple bounding box on image
        xmin_GT = self.gt.loc[self.pointer,'xmin']
        ymin_GT = self.gt.loc[self.pointer,'ymin']
        xmax_GT = self.gt.loc[self.pointer,'xmax']
        ymax_GT = self.gt.loc[self.pointer,'ymax']
        lesion_radius = np.amin([0.5*(xmax_GT - xmin_GT),0.5*(ymax_GT - ymin_GT)])

        if os.path.isfile(breast_contour_filename):
            breast_centroid,breast_contour,contour_x,contour_y = clock_angle_overlay.get_breast_outline(breast_contour_filename)
            #overlay breast contour on image
            #im_overlay = cv2.polylines(im_overlay, np.array(breast_contour['contour']), True, (0,255,255), 3)

            #is nipple pointing left or right?
            nipple_pointing = clock_angle_overlay.get_nipple_pointing(nipple,breast_centroid)

            #get clock center and radius
            image_size = im_overlay.shape
            #image_size = (image_size1[1],image_size1[0],3)
            clock = clock_angle_overlay.get_clock(image_size,contour_x,contour_y,nipple,nipple_pointing)

            #overlay clock on chest wall
            contour_y_min = np.amin(contour_y)
            contour_y_max = np.amax(contour_y)
            #im_overlay = cv2.line(im_overlay, (clock.x,contour_y_min), (clock.x,contour_y_max),  (0,255,0), 10)

        #if (not pd.isnull(gt.loc[self.pointer,'clock_angle'])) & (not pd.isnull(gt.loc[self.pointer,'distance'])):
            #if gt.loc[self.pointer,'distance'] != 'RTA':
        clock_angle = gt.loc[self.pointer,'clock_angle']
        cmfn        = float(gt.loc[self.pointer,'distance'])
        lesion1      = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)

        #compute and draw protrator 
        clock_preset = np.array(['12:00','11:00','10:00','9:00','8:00','7:00','6:00','5:00','4:00','3:00','2:00','1:00']).flatten()
        cmfn_preset  = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]).flatten()

        #im_overlay = cv2.polylines(im_overlay, curves, True, (0,255,255), 1)

        #compute curves with preset cmfn
        curves2 = []
        for cmfn in cmfn_preset:
            curve = []
            for clock_angle in clock_preset:
                lesion      = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
                pt          = []
                pt.append(int(round(lesion.x)))
                pt.append(int(round(lesion.y)))
                pt = np.array(pt).flatten()
                curve.append(pt)
            curves2.append(curve)
        curves2 = np.array(curves2)
        #im_overlay = cv2.polylines(im_overlay, curves2, True, (0,255,255), 1)        

        return clock,contour_y_min,contour_y_max,lesion1, lesion_radius,xmin_GT,ymin_GT,xmax_GT,ymax_GT,xmin,ymin,xmax,ymax,curves2,nipple,image_laterality,view_position,nipple_pointing,pixel_size

    def overlay_all(self):
        self.clear_earlier_image()
        clock,contour_y_min,contour_y_max,lesion, lesion_radius,xmin_GT,ymin_GT,xmax_GT,ymax_GT,xmin,ymin,xmax,ymax,curves2,nipple,image_laterality,view_position,nipple_pointing,pixel_size = self.get_values()
        sopuid,dcm_filename,png_filename1,png_filename2,png_filename3,nipple_filename,breast_contour_filename = self.filenames(self.gt,self.pointer)
        image1 = im_overlay.copy()
        image1 = cv2.rectangle(image1,(int(xmin),int(ymin)),(int(xmax),int(ymax)),(255,0,0),2)#Nipple location
        #n_col = image1.shape[1]
        #n_row = image1.shape[0]
        image1 = cv2.rectangle(image1,(int(xmin_GT),int(ymin_GT)),(int(xmax_GT),int(ymax_GT)),(0,0,255),2)#Ground truth location
        #image1 = cv2.rectangle(image1,(clock.x,contour_y_min), (clock.x,contour_y_max),  (0,255,0), 10)#Clock on chest wall
        if (not math.isnan(lesion.x)) & (not math.isnan(lesion.y)):
            image1  = cv2.circle(image1, (int(lesion.x),int(lesion.y)), int(lesion_radius),  (0,255,0), 10)
        
        cv2.imwrite(png_filename1,image1)
        self.image_ori = image1
        del self.image
        image1 = cv2.resize(image1,(Size1,Size2))
        n_col = Size2
        n_row = Size1
        self.image = image1
        self.image = Image.fromarray(self.image)
        self.image = ImageTk.PhotoImage(self.image)
        self.show_image(self.Frame3,n_col,n_row)
    
    def overlay_protractor(self):
        self.clear_earlier_image()
        clock,contour_y_min,contour_y_max,lesion, lesion_radius,xmin_GT,ymin_GT,xmax_GT,ymax_GT,xmin,ymin,xmax,ymax,curves2,nipple,image_laterality,view_position,nipple_pointing,pixel_size = self.get_values()
        sopuid,dcm_filename,png_filename1,png_filename2,png_filename3,nipple_filename,breast_contour_filename = self.filenames(self.gt,self.pointer)
        clock_preset = np.array(['12:00','11:00','10:00','9:00','8:00','7:00','6:00','5:00','4:00','3:00','2:00','1:00']).flatten()
        cmfn_preset  = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]).flatten()
        image2 = im_overlay.copy()
        #n_col = image2.shape[1]
        #n_row = image2.shape[0]
        label_position_x = []
        label_position_y = []
        curves = []
        for clock_angle in clock_preset:
            curve = []
            for cmfn in cmfn_preset:
                lesion      = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
                pt          = []
                pt.append(int(round(lesion.x)))
                pt.append(int(round(lesion.y)))
                pt = np.array(pt).flatten()
                curve.append(pt)
            curves.append(curve)
            x_min      = curves[-1][5][0]
            y_min      = curves[-1][5][1]
            found = np.array(np.where((label_position_x == x_min) & (label_position_y == y_min))).flatten()
            if len(found) == 0:
                image2 = cv2.putText(image2,clock_angle,(x_min,y_min-5), cv2.FONT_HERSHEY_SIMPLEX, 1.75,(255,255,0),2,cv2.LINE_AA)
                label_position_x.append(x_min)
                label_position_y.append(y_min)
            else:
                x_min      = curves[-1][7][0]
                y_min      = curves[-1][7][1]
                image2 = cv2.putText(image2,clock_angle,(x_min,y_min-5), cv2.FONT_HERSHEY_SIMPLEX, 1.75,(255,255,0),2,cv2.LINE_AA)
        curves = np.array(curves)
        image2 = cv2.rectangle(image2,(clock.x,contour_y_min), (clock.x,contour_y_max),  (0,255,0), 10)#Clock on chest wall
        image2 = cv2.polylines(image2, curves, True, (0,255,255), 2)
        image2 = cv2.polylines(image2, curves2, True, (0,255,255), 2) 
        cv2.imwrite(png_filename2,image2)
        self.image_ori = image2
        del self.image
        image2 = cv2.resize(image2,(Size1,Size2))
        n_col = Size2
        n_row = Size1
        self.image = image2
        self.image = Image.fromarray(self.image)
        self.image = ImageTk.PhotoImage(self.image)
        self.show_image(self.Frame3,n_col,n_row)

    def overlay_protractor_and_GT(self):
        self.clear_earlier_image()
        #GT
        clock,contour_y_min,contour_y_max,lesion, lesion_radius,xmin_GT,ymin_GT,xmax_GT,ymax_GT,xmin,ymin,xmax,ymax,curves2,nipple,image_laterality,view_position,nipple_pointing,pixel_size = self.get_values()
        sopuid,dcm_filename,png_filename1,png_filename2,png_filename3,nipple_filename,breast_contour_filename = self.filenames(self.gt,self.pointer)
        image3 = im_overlay.copy()
        image3 = cv2.rectangle(image3,(int(xmin),int(ymin)),(int(xmax),int(ymax)),(255,0,0),2)#Nipple location
        #n_col = image1.shape[1]
        #n_row = image1.shape[0]
        image3 = cv2.rectangle(image3,(int(xmin_GT),int(ymin_GT)),(int(xmax_GT),int(ymax_GT)),(0,0,255),2)#Ground truth location
        #image1 = cv2.rectangle(image1,(clock.x,contour_y_min), (clock.x,contour_y_max),  (0,255,0), 10)#Clock on chest wall
        if (not math.isnan(lesion.x)) & (not math.isnan(lesion.y)):
            image3  = cv2.circle(image3, (int(lesion.x),int(lesion.y)), int(lesion_radius),  (0,255,0), 10)
        #Protractor
        clock_preset = np.array(['12:00','11:00','10:00','9:00','8:00','7:00','6:00','5:00','4:00','3:00','2:00','1:00']).flatten()
        cmfn_preset  = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]).flatten()
        label_position_x = []
        label_position_y = []
        curves = []
        for clock_angle in clock_preset:
            curve = []
            for cmfn in cmfn_preset:
                lesion      = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
                pt          = []
                pt.append(int(round(lesion.x)))
                pt.append(int(round(lesion.y)))
                pt = np.array(pt).flatten()
                curve.append(pt)
            curves.append(curve)
            x_min      = curves[-1][5][0]
            y_min      = curves[-1][5][1]
            found = np.array(np.where((label_position_x == x_min) & (label_position_y == y_min))).flatten()
            if len(found) == 0:
                image3 = cv2.putText(image3,clock_angle,(x_min,y_min-5), cv2.FONT_HERSHEY_SIMPLEX, 1.75,(255,255,0),4,cv2.LINE_AA)
                label_position_x.append(x_min)
                label_position_y.append(y_min)
            else:
                x_min      = curves[-1][7][0]
                y_min      = curves[-1][7][1]
                image3 = cv2.putText(image3,clock_angle,(x_min,y_min-5), cv2.FONT_HERSHEY_SIMPLEX, 1.75,(255,255,0),4,cv2.LINE_AA)
        curves = np.array(curves)
        image3 = cv2.rectangle(image3,(clock.x,contour_y_min), (clock.x,contour_y_max),  (0,255,0), 10)#Clock on chest wall
        image3 = cv2.polylines(image3, curves, True, (0,255,255), 2)
        image3 = cv2.polylines(image3, curves2, True, (0,255,255), 2)     
        cv2.imwrite(png_filename3,image3)
        self.image_ori = image3
        del self.image
        image3 = cv2.resize(image3,(Size1,Size2))
        n_col = Size2
        n_row = Size1
        self.image = image3
        self.image = Image.fromarray(self.image)
        self.image = ImageTk.PhotoImage(self.image)
        self.show_image(self.Frame3,n_col,n_row)

    def getbackClockAngle_and_distance(self,x,y):
        clock,contour_y_min,contour_y_max,lesion, lesion_radius,xmin_GT,ymin_GT,xmax_GT,ymax_GT,xmin,ymin,xmax,ymax,curves2,nipple,image_laterality,view_position,nipple_pointing,pixel_size = self.get_values()
        x_in = 0
        y_in = 0
        x = (x/Size1)*self.n_col
        y = (y/Size2)*self.n_row
        self.canvas.delete(self.item3)
        view = view_position
        laterality = image_laterality
        if nipple_pointing == 'r':
            if (view == 'mlo') | (view == 'ml'):
                dx = x - nipple.x
                dy = y - nipple.y
                slope = dy/dx
                cmfn = (((-dx)*np.sqrt(1 + slope * slope))*(pixel_size))/10.0
                y_in = -(slope*(clock.x - nipple.x) + nipple.y - clock.y)
                #print y_in
            elif view == 'cc':
                if laterality == 'l':
                    dx = x - nipple.x
                    dy = y - nipple.y
                    slope = dy/dx
                    cmfn = (((-dx)*np.sqrt(1 + slope * slope))*(pixel_size))/10.0
                    x_in = -(slope*(clock.x - nipple.x) + nipple.y - clock.y)
                    #print x_in
                else:
                    dx = x - nipple.x
                    dy = y - nipple.y
                    slope = dy/dx
                    cmfn = (((-dx)*np.sqrt(1 + slope * slope))*(pixel_size))/10.0
                    x_in = slope*(clock.x - nipple.x) + nipple.y - clock.y
                    #print x_in    
            else:
                print 'invalid view: ',view
        elif nipple_pointing == 'l':
            if (view == 'mlo') | (view == 'ml'):
                dx = x - nipple.x
                dy = y - nipple.y
                slope = dy/dx
                cmfn = (((dx)*np.sqrt(1 + slope * slope))*(pixel_size))/10.0    
                y_in = -(slope*(clock.x - nipple.x) + nipple.y - clock.y)
                #print y_in
            elif view == 'cc':
                if laterality == 'l':
                    dx = x - nipple.x
                    dy = y - nipple.y
                    slope = dy/dx
                    cmfn = (((dx)*np.sqrt(1 + slope * slope))*(pixel_size))/10.0  
                    x_in = -(slope*(clock.x - nipple.x) + nipple.y - clock.y)
                    #print x_in
                else: 
                    dx = x - nipple.x
                    dy = y - nipple.y
                    slope = dy/dx
                    cmfn = (((dx)*np.sqrt(1 + slope * slope))*(pixel_size))/10.0
                    x_in = slope*(clock.x - nipple.x) + nipple.y - clock.y
                    #print x_in
            else:
                print 'invalid view: ',view

        if x_in == 0:
            angle = np.arcsin(y_in/clock.r)
            #print (180/np.pi)*angle
            if angle > 0:
                angle = 0.5* np.pi - angle
            elif angle < 0: 
                angle = 0.5* np.pi + abs(angle)
            else:
                angle = angle
            hours = (6/np.pi)*angle
            minutes = (hours - math.trunc(hours))*60

        elif y_in == 0:
            angle = np.arccos(x_in/clock.r)
            #print (180/np.pi)*angle
            hours = (6/np.pi)*angle + 3
            minutes = (hours - math.trunc(hours))*60

        tmp = 0   
        if (view == 'mlo') | (view == 'ml'):
            hours2 = 12 - int(hours)
            minutes2 = minutes
        if view == 'cc':
            if hours == 6:
                hours2 = 12
            elif hours > 6:
                tmp = int(hours) - 6
                hours2 = 12 - tmp
            else: 
                tmp = int(hours) - 3
                print tmp
                hours2 = 3 - tmp
                print hours2
            minutes2 = minutes
        print "Distance :%f"%cmfn
        print "Hours :%d Minutes :%.2f"%(hours,minutes)
        print "Hours2 :%d Minutes2 :%.2f"%(hours2,minutes2)
        x = (x/self.n_col)*1600
        y = (y/self.n_row)*2400
        minutesrounded = 0
        if int(minutes) < 15:
            minutesrounded = 0
        elif int(minutes) > 15:
            minutesrounded = 30
        elif int(minutes) > 30 and int(minutes) < 45:
            minutesrounded = 30
        elif int(minutes) > 45:
            hours = hours + 1
            minutes = 0
        var1 = "Hours: {0}, Minutes: {1}".format(int(hours),minutesrounded)
        if nipple_pointing == 'l':
            a = ((nipple.x)/self.n_col)*Size1
            b = ((nipple.y)/self.n_row)*Size2
            self.item3 = self.canvas.create_text(a - 300,b,fill="White",font="Times 20 bold",
                        text=var1, tags = "text")
        else:
            a = ((nipple.x)/self.n_col)*Size1
            b = ((nipple.y)/self.n_row)*Size2
            self.item3 = self.canvas.create_text(a + 3  00,b,fill="White",font="Times 20 bold",
                        text=var1, tags = "text")
        
        return cmfn, hours

    def save(self):
        clock,contour_y_min,contour_y_max,lesion, lesion_radius,xmin_GT,ymin_GT,xmax_GT,ymax_GT,xmin,ymin,xmax,ymax,curves2,nipple,image_laterality,view_position,nipple_pointing,pixel_size = self.get_values()
        sopuid,dcm_filename,png_filename1,png_filename2,png_filename3,nipple_filename,breast_contour_filename = self.filenames(self.gt,self.pointer)
        data = {}
        data['sopuid'] = sopuid
        data['image_laterality'] = image_laterality
        data['NRows'] = self.n_row
        data['NColumns'] = self.n_col
        data['pixel_spacing'] = pixel_size
        data['nipple_pointing'] = nipple_pointing
        data['view_position'] = view_position
        data['Original_lesion_x'] = lesion.x
        data['Original_lesion_y'] = lesion.y
        data['Modified_mass_lesion_x'] = self.ML_x
        data['Modified_mass_lesion_y'] = self.ML_y
        data['Modified_nipple_location_x'] = self.NL_x
        data['Modified_nipple_location_y'] = self.NL_y
        data['Modified_groundtruth_location_x'] = self.GT_x
        data['Modified_groundtruth_location_y'] = self.GT_y
        data['Size1-Row'] = Size1
        data['Size2-Column'] = Size2

        str1 = self.Dir + "outputsJson/" +  "{0}.json".format(sopuid)
        with open(str1,'w') as outfile:
            json.dump(data,outfile)

    def rerender_protractor(self,x,y):
        self.clear_earlier_image()
        #GT
        clock,contour_y_min,contour_y_max,lesion, lesion_radius,xmin_GT,ymin_GT,xmax_GT,ymax_GT,xmin,ymin,xmax,ymax,curves2,nipple,image_laterality,view_position,nipple_pointing,pixel_size = self.get_values()
        sopuid,dcm_filename,png_filename1,png_filename2,png_filename3,nipple_filename,breast_contour_filename = self.filenames(self.gt,self.pointer)
        image3 = im_overlay.copy()
        x = (x/Size1)*self.n_col
        y = (y/Size2)*self.n_row
        nipple_new = POINT(x,y)
        nipple = nipple_new
        image3 = cv2.rectangle(image3,(int(xmin),int(ymin)),(int(xmax),int(ymax)),(255,0,0),2)#Nipple location
        #n_col = image1.shape[1]
        #n_row = image1.shape[0]
        image3 = cv2.rectangle(image3,(int(xmin_GT),int(ymin_GT)),(int(xmax_GT),int(ymax_GT)),(0,0,255),2)#Ground truth location
        #image1 = cv2.rectangle(image1,(clock.x,contour_y_min), (clock.x,contour_y_max),  (0,255,0), 10)#Clock on chest wall
        if (not math.isnan(lesion.x)) & (not math.isnan(lesion.y)):
            image3  = cv2.circle(image3, (int(lesion.x),int(lesion.y)), int(lesion_radius),  (0,255,0), 10)
        #Protractor
        clock_preset = np.array(['12:00','11:00','10:00','9:00','8:00','7:00','6:00','5:00','4:00','3:00','2:00','1:00']).flatten()
        cmfn_preset  = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]).flatten()
        label_position_x = []
        label_position_y = []
        curves = []
        for clock_angle in clock_preset:
            curve = []
            for cmfn in cmfn_preset:
                lesion      = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
                pt          = []
                pt.append(int(round(lesion.x)))
                pt.append(int(round(lesion.y)))
                pt = np.array(pt).flatten()
                curve.append(pt)
            curves.append(curve)
            x_min      = curves[-1][5][0]
            y_min      = curves[-1][5][1]
            found = np.array(np.where((label_position_x == x_min) & (label_position_y == y_min))).flatten()
            if len(found) == 0:
                image3 = cv2.putText(image3,clock_angle,(x_min,y_min-5), cv2.FONT_HERSHEY_SIMPLEX, 1.75,(255,255,0),2,cv2.LINE_AA)
                label_position_x.append(x_min)
                label_position_y.append(y_min)
            else:
                x_min      = curves[-1][7][0]
                y_min      = curves[-1][7][1]
                image3 = cv2.putText(image3,clock_angle,(x_min,y_min-5), cv2.FONT_HERSHEY_SIMPLEX, 1.75,(255,255,0),2,cv2.LINE_AA)
        curves = np.array(curves)

        curves2 = []
        for cmfn in cmfn_preset:
            curve = []
            for clock_angle in clock_preset:
                lesion      = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
                pt          = []
                pt.append(int(round(lesion.x)))
                pt.append(int(round(lesion.y)))
                pt = np.array(pt).flatten()
                curve.append(pt)
            curves2.append(curve)
        curves2 = np.array(curves2)

        image3 = cv2.rectangle(image3,(clock.x,contour_y_min), (clock.x,contour_y_max),  (0,255,0), 10)#Clock on chest wall
        image3 = cv2.polylines(image3, curves, True, (0,255,255), 2)
        image3 = cv2.polylines(image3, curves2, True, (0,255,255), 2)     
        cv2.imwrite(png_filename3,image3)
        self.image_ori = image3
        del self.image
        image3 = cv2.resize(image3,(Size1,Size2))
        n_col = Size2
        n_row = Size1
        self.image = image3
        self.image = Image.fromarray(self.image)
        self.image = ImageTk.PhotoImage(self.image)
        self.show_image(self.Frame3,n_col,n_row)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = 'check accuracy of annotators (measured by ratio of overlap/union between \
    gt bounding box and annotated bounding box')
    parser.add_argument('-gt', dest = 'gt', help = 'directory containting gt PR files', default = './gt_pr')
    parser.add_argument('-Dir', dest = 'Dir', help = 'Main directory', default = './gt_pr')
    args = parser.parse_args()
    gt_csv = args.gt
    gt = pd.read_csv(gt_csv)
    Dir = args.Dir

    root = Tk()
    root.geometry("2400x2200")
    pointer = 0
    my_gui = MyFirstGUI(root,gt,Dir,pointer)
    #my_gui.next_dicom(root,gt,0)
    root.mainloop()