import os
import glob
import json 
from Tkinter import Tk, Label, Button
from Tkinter import *
from PIL import ImageTk, Image
import pandas as pd
import argparse
import pydicom
import numpy as np
import cv2


class MyFirstGUI:
    def __init__(self, master,gt,Dir,pointer):
        self.master = master
        self.gt = gt
        self.Dir = Dir
        self.pointer = pointer
        master.title("INTERACTIVE SCRIPT")
        self.main_window()

    def main_window(self):
        self.label = Label(self.master, text="This is our first GUI!")     
        self.Frame1 = Frame(self.master, highlightbackground = "green", highlightthickness = 1,width = 800, height = 1000, bd =0)
        self.Frame1.grid(row = 0, column = 0, rowspan = 1000, columnspan = 800, sticky = W+E+N+S) 
        self.greet_button = Button(self.Frame1, text="Render clock angle and GT markings", command=self.greet).grid(row=1,column=1,sticky=E+W)
        #self.close_button = Button(Frame1, text="Close", command=master.quit).grid(row=999,column=2,sticky=E+W)
        self.get_sopuid_clockangel_cmfn()
        
        self.Frame2 = Frame(self.master, highlightbackground = "blue", highlightthickness = 1,width = 800, height = 1000)
        self.Frame2.grid(row = 1001, column = 0, rowspan = 2000, columnspan = 800, sticky = W+E+N+S)
        self.another_button = Button(self.Frame2, text = "Render protractor over the image").grid(row=1001, column=1,sticky = N+E+W+S)
        
        self.Frame3 = Frame(self.master, highlightbackground = "yellow", highlightthickness = 1, width = 1600, height = 2000)
        self.Frame3.grid(row = 0, column = 801, rowspan = 2000, columnspan = 2600, sticky = W+E+N+S)
        #self.random_button = Button(Frame3,text = "Button in frame 3").grid(row = 1999, column = 1, sticky = N+E+W+S)
        self.First_Time = True
        self.create_image(self.master,self.gt,self.pointer,self.Frame3)
        
        next_button = Button(self.Frame1, text ="Next", command = self.next_dicom).grid(row = 10, column = 2)
        back_button = Button(self.Frame1, text = "Back", command = self.previous_dicom).grid(row = 11,column = 2)
        #path = "/mnt/Array/share/users/anirudh/Images/Mass/"
        #os.chdir(path)
    
    def get_sopuid_clockangel_cmfn(self):
        self.sopuid = gt.loc[self.pointer,'sop']
        self.clock_angle = self.gt.loc[self.pointer,'clock_angle']
        self.cmfn   = float(self.gt.loc[self.pointer,'distance'])
        Label(self.Frame1, text = "SOP-UID : {}".format(self.sopuid)).grid(row = 6, column = 1, sticky = W)
        Label(self.Frame1, text = "Clock Angel : {}".format(self.clock_angle)).grid(row=7, column = 1, sticky = W )
        Label(self.Frame1, text = "Distance in cms : {}".format(self.cmfn)).grid(row=8, column = 1, sticky = W)
        #return sopuid, clock_angle, cmfn 

    def greet(self):
        print("Clock angle and markings rendered!")

    def next_dicom(self):
        length = len(self.gt['sop'])
        if self.First_Time != True:
            #self.Frame3.grid_forget()
            if self.pointer != length:
                self.pointer = self.pointer + 1
            else:
                self.pointer = self.pointer
            self.clear_earlier_image()
            self.create_image(self.master,self.gt,self.pointer,self.Frame3)
            self.get_sopuid_clockangel_cmfn()
            print(self.pointer)   
        else :
            self.First_Time = False
            print('why god why')
        #return pointer

    def previous_dicom(self):
        #if self.First_Time == False:
        #    self.First_Time = False
        #    print('previous')
        #else:
        #self.Frame3.grid_forget()
        if self.pointer != 0:
            self.pointer = self.pointer - 1
        else:
            self.pointer = self.pointer
        self.clear_earlier_image()
        self.create_image(self.master,self.gt,self.pointer,self.Frame3)
        self.get_sopuid_clockangel_cmfn()
        #return pointer

    def filenames(self,gt,pointer):
        sopuid = gt.loc[pointer,'sop']
        #print(Dir)
        dcm_filename = Dir + 'DCMs/' + sopuid + '.dcm'
        png_filename1 = Dir + 'outputs/' + sopuid +'._1_png'
        png_filename2 = Dir + 'outputs/' + sopuid + '_2_.png'
        nipple_filename = Dir + 'JSON-Outputs/' + sopuid + '_MassCalc.json'
        breast_contour_filename = Dir + 'BreastBoundary_Output/' + sopuid + '.json'
        return sopuid,dcm_filename,png_filename1,png_filename2,nipple_filename,breast_contour_filename

    def create_image(self,master,gt,pointer,Frame3):
        sopuid,dcm_filename,png_filename1,png_filename2,nipple_filename,breast_contour_filename = self.filenames(gt,pointer)
        if os.path.isfile(dcm_filename):
            #read DCM
            print 'reading: ',dcm_filename
            dicom_object = pydicom.dcmread(dcm_filename)
            im = np.array((float(255)/float(4096))* np.array(dicom_object.pixel_array[:,:]).astype('float')).astype('uint8')
            #get image size
            image_size = im.shape
            n_row      = image_size[0]
            n_col      = image_size[1]
            im_resized = cv2.resize(im, (n_col,n_row))
            #store image in im_overlay (3 channels image overlay array)
            im_overlay = np.zeros((n_row,n_col,3)).astype('uint8')
            im_overlay[:,:,0] = np.copy(im_resized)
            im_overlay[:,:,1] = np.copy(im_resized)
            im_overlay[:,:,2] = np.copy(im_resized)
            #Converting image into PIL format so that it can be shown in the GUI
            self.image = im_overlay
            self.image = Image.fromarray(self.image)
            self.image = ImageTk.PhotoImage(self.image)
            #self.Frame3 = Frame(self.master, highlightbackground = "yellow", highlightthickness = 1, width = 1600, height = 2000)
            #self.Frame3.grid(row = 0, column = 801, rowspan = 2000, columnspan = 2600, sticky = W+E+N+S)
            self.panel = Label(Frame3,image = self.image)
            self.panel.image = self.image
            self.panel.grid(row = 0, column = 801)
            #self.next_dicom()
            #return im_overlay
    def clear_earlier_image(self):
            self.panel['image'] = ''

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description = 'check accuracy of annotators (measured by ratio of overlap/union between \
    gt bounding box and annotated bounding box')

    parser.add_argument('-gt', dest = 'gt', help = 'directory containting gt PR files', default = './gt_pr')
    parser.add_argument('-Dir', dest = 'Dir', help = 'Main directory', default = './gt_pr')
    args = parser.parse_args()
    gt_csv = args.gt
    gt = pd.read_csv(gt_csv)
    Dir = args.Dir

    root = Tk()
    #root.geometry("2600x2000")
    pointer = 0
    my_gui = MyFirstGUI(root,gt,Dir,pointer)
    #my_gui.next_dicom(root,gt,0)
    root.mainloop()