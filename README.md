This repository is for monitoring the progress of the interactive script for overlaying clock angle markings and ground truth markings on each other. 

Method to run this script: 
python interactive_Script_canvas.py -gt location/to/gt.csv -Dir location/to/dir/where/dicoms/are/removed/into/JSONS/PNGs

-gt refers to the csv that has the clock, angle and distance
Example : /mnt/Array/share/users/anirudh/interactive_script_for_overlaying_clock_angle/Data/gt_n_clock_position.csv
-Dir refers to the directory which has the breast boundary jsons, nipple locations , PNG images from Dicoms etc 
Example : /mnt/Array/share/users/anirudh/interactive_script_for_overlaying_clock_angle/Data/


Make sure to have the following libraries: 
Tkinter,PIL,pandas, pydicom, numpy,tkFont, cv2, glob, json and argparse