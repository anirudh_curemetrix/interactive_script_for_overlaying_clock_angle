#Author: Hoanh Vu
#CureMetrix, Inc.
#May 2019

import numpy as np
import pandas as pd
import pydicom
import matplotlib.pyplot as plt
import os
import glob
import cv2
import matplotlib.pyplot as plt
import json
import argparse

class ROI:
        def __init__(self,file,sopuid,version,image_laterality,view_position,pixel_size,patient_id,mass,calcs,cmAssist_error, \
        nROIs_mass ,qvalue_mass_min ,qvalue_mass_max ,qvalue_mass_mean, \
        nROIs_calcs,qvalue_calcs_min,qvalue_calcs_max,qvalue_calcs_mean, \
        combinedScoreRaw_mass_min ,combinedScoreRaw_mass_max ,combinedScoreRaw_mass_mean, \
        combinedScoreRaw_calcs_min,combinedScoreRaw_calcs_max,combinedScoreRaw_calcs_mean):
                self.filename                    = file
                self.sopuid                      = sopuid
                self.version                     = version
                self.image_laterality            = image_laterality
                self.view_position               = view_position    
                self.pixel_size                  = pixel_size
                self.patient_id                  = patient_id
                self.mass                        = mass
                self.calcs                       = calcs
                self.cmAssist_error              = cmAssist_error
                self.nROIs_mass                  = nROIs_mass
                self.qvalue_mass_min             = qvalue_mass_min
                self.qvalue_mass_max             = qvalue_mass_max
                self.qvalue_mass_mean            = qvalue_mass_mean
                self.nROIs_calcs                 = nROIs_calcs
                self.qvalue_calcs_min            = qvalue_calcs_min
                self.qvalue_calcs_max            = qvalue_calcs_max
                self.qvalue_calcs_mean           = qvalue_calcs_mean
                self.combinedScoreRaw_mass_min   = combinedScoreRaw_mass_min
                self.combinedScoreRaw_mass_max   = combinedScoreRaw_mass_max
                self.combinedScoreRaw_mass_mean  = combinedScoreRaw_mass_mean
                self.combinedScoreRaw_calcs_min  = combinedScoreRaw_calcs_min
                self.combinedScoreRaw_calcs_max  = combinedScoreRaw_calcs_max
                self.combinedScoreRaw_calcs_mean = combinedScoreRaw_calcs_mean

class POINT:
        def __init__(self,x,y):
                self.x = x
                self.y = y

class CLOCK:
        def __init__(self,x,y,r):
                self.x = x
                self.y = y
                self.r = r

class PR:
        def __init__(self,filename,sopuid):
                self.filename                = filename
                self.sopuid                  = sopuid

def get_nipple(filename):
	#get nipple
	nipple_roi = get_json(filename)

	#get image laterality
	image_laterality = np.string_(nipple_roi.image_laterality).lower()

	#get view position
	view_position    = np.string_(nipple_roi.view_position).lower()

	#get pixel size
	pixel_size       = float(nipple_roi.pixel_size)

	score = []
	xmin  = []
	ymin  = []
	xmax  = []
	ymax  = []
	for idx,roi in enumerate(nipple_roi.mass):
		#print idx,roi['qvalue'],roi['xmin'],roi['ymin'],roi['xmax'],roi['ymax']
		score.append(roi['qvalue'])
		xmin.append(roi['min']['x'])
		ymin.append(roi['min']['y'])
		xmax.append(roi['max']['x'])
		ymax.append(roi['max']['y'])
	score = np.array(score).flatten()
	xmin  = np.array(xmin).flatten()
	ymin  = np.array(ymin).flatten()
	xmax  = np.array(xmax).flatten()
	ymax  = np.array(ymax).flatten()

	#select roi with maximum score; nipple bounding box = [xmin,ymin,xmax,ymax]
	argmax = np.argmax(score)
	xmin   = xmin[argmax]
	ymin   = ymin[argmax]
	xmax   = xmax[argmax]
	ymax   = ymax[argmax]

	#nipple's coordinates
	x = 0.5*(xmin+xmax)
	y = 0.5*(ymin+ymax)
	nipple   = POINT(x,y)
	return image_laterality,view_position,pixel_size, xmin,ymin,xmax,ymax,nipple

def get_breast_outline(filename):
	#get breast outline
       	with open(filename) as json_file:
            	breast_contour = json.load(json_file)
		contour_x = []
		contour_y = []
		for p in range(len(breast_contour['contour'])):
			contour_x.append(breast_contour['contour'][p][0][0])	
			contour_y.append(breast_contour['contour'][p][0][1])	
		contour_x  = np.array(contour_x).flatten()
		contour_y  = np.array(contour_y).flatten()
		x          = np.sum(contour_x)/len(contour_x)
		y          = np.sum(contour_y)/len(contour_y)

		#breast's centroid
		breast_centroid = POINT(x,y)

	return breast_centroid,breast_contour,contour_x,contour_y

def get_nipple_pointing(nipple,breast_centroid):
	#if breast centroid is to the left of nipple ---> nipple points right. Otherwise, nipple points left
	if breast_centroid.x < nipple.x:
		nipple_pointing = 'r'
	else:
		nipple_pointing = 'l'
	return nipple_pointing

def get_clock(image_size,contour_x,contour_y,nipple,nipple_pointing):
	#image size: scale to the correct image size
	xmin = 0
	xmax = image_size[1]
	ymin = 0
	ymax = image_size[0]
	#get breast's dimension at the boundary of image
	contour_y_max  = np.amax(contour_y)
	contour_y_min  = np.amin(contour_y)

	clock_radius   = 0.5*(contour_y_max - contour_y_min)
	#method 1: clock center is the mid point between points where the breast boundary intersects the image edge
	#clock_center_y = 0.5*(contour_y_max + contour_y_min)
	#method 2: clock is centered ar nipple level
	clock_center_y = nipple.y

	if nipple_pointing == 'l':
		clock_center_x = xmax
	else:
		clock_center_x = xmin
	clock = CLOCK(clock_center_x,clock_center_y,clock_radius)
	return clock


def get_json(file):
        #import json from a single json file
        calcs = []
        mass  = []
        qvalue_mass  = []
        qvalue_calcs = []
        qvalue_mass_max   = -1
        qvalue_calcs_max  = -1
        qvalue_mass_min   = -1
        qvalue_calcs_min  = -1
        qvalue_mass_mean  = -1
        qvalue_calcs_mean = -1
        combinedScoreRaw_mass  = []
        combinedScoreRaw_calcs = []
        combinedScoreRaw_mass_max   = -1
        combinedScoreRaw_calcs_max  = -1
        combinedScoreRaw_mass_min   = -1
        combinedScoreRaw_calcs_min  = -1
        combinedScoreRaw_mass_mean  = -1
        combinedScoreRaw_calcs_mean = -1

        with open(file) as json_file:
                data = json.load(json_file)
                version = data['q_version'] + '.' + data['q_build']
		image_laterality = data['imageLaterality']
		view_position    = data['viewPosition']
		pixel_size = data['pixelSize']
                if not ('patientId' in data):
                        cmAssist_error = True
                        patient_id = 'UNKNOWN'
                        sopuid = data['filepathname'].replace('.dcm','')
                else:
                        cmAssist_error = False
                        patient_id = data['patientId']
                        sopuid     = data['sop_instance_uid']
                        if len(data['ROI']) > 0:
                                for j in range(len(data['ROI'])):
                                        if data['ROI'][j]['c_m'] == 'M':
                                                mass.append(data['ROI'][j])
                                                qvalue_mass.append(data['ROI'][j]['qvalue'])
                                                combinedScoreRaw_mass.append(data['ROI'][j]['combinedScoreRaw'])
                                        elif data['ROI'][j]['c_m'] == 'C':
                                                calcs.append(data['ROI'][j])
                                                qvalue_calcs.append(data['ROI'][j]['qvalue'])
                                                combinedScoreRaw_calcs.append(data['ROI'][j]['combinedScoreRaw'])
                                        else:
                                                raise Exception('lesion type [' + data['ROI'][j]['c_m'] + '] not implemented')
                                qvalue_mass       = np.array(qvalue_mass).flatten()
                                if len(qvalue_mass) > 0:
                                        qvalue_mass_min   = np.amin(qvalue_mass)
                                        qvalue_mass_max   = np.amax(qvalue_mass)
                                        qvalue_mass_mean  = np.mean(qvalue_mass)

                                qvalue_calcs      = np.array(qvalue_calcs).flatten()
                                if len(qvalue_calcs) > 0:
                                        qvalue_calcs_max  = np.amax(qvalue_calcs)
                                        qvalue_calcs_min  = np.amin(qvalue_calcs)
                                        qvalue_calcs_mean = np.mean(qvalue_calcs)

                                combinedScoreRaw_mass       = np.array(combinedScoreRaw_mass).flatten()
                                if len(combinedScoreRaw_mass) > 0:
                                        combinedScoreRaw_mass_min   = np.amin(combinedScoreRaw_mass)
                                        combinedScoreRaw_mass_max   = np.amax(combinedScoreRaw_mass)
                                        combinedScoreRaw_mass_mean  = np.mean(combinedScoreRaw_mass)

                                combinedScoreRaw_calcs      = np.array(combinedScoreRaw_calcs).flatten()
                                if len(combinedScoreRaw_calcs) > 0:
                                        combinedScoreRaw_calcs_max  = np.amax(combinedScoreRaw_calcs)
                                        combinedScoreRaw_calcs_min  = np.amin(combinedScoreRaw_calcs)
                                        combinedScoreRaw_calcs_mean = np.mean(combinedScoreRaw_calcs)
        nROIs_mass  = len(qvalue_mass)
        nROIs_calcs = len(qvalue_calcs)

        json_info = ROI(file,sopuid,version,image_laterality,view_position,pixel_size,patient_id,mass,calcs,cmAssist_error, \
        nROIs_mass,qvalue_mass_min ,qvalue_mass_max ,qvalue_mass_mean, \
        nROIs_calcs,qvalue_calcs_min,qvalue_calcs_max,qvalue_calcs_mean, \
        combinedScoreRaw_mass_min ,combinedScoreRaw_mass_max ,combinedScoreRaw_mass_mean, \
        combinedScoreRaw_calcs_min,combinedScoreRaw_calcs_max,combinedScoreRaw_calcs_mean)
        return json_info

def get_iou(gt_xmin,gt_xmax,gt_ymin,gt_ymax,annotator_xmin, annotator_xmax,annotator_ymin,annotator_ymax):
	xmin = np.amax(np.array([gt_xmin,annotator_xmin]).flatten())
	xmax = np.amin(np.array([gt_xmax,annotator_xmax]).flatten())
	ymin = np.amax(np.array([gt_ymin,annotator_ymin]).flatten())
	ymax = np.amin(np.array([gt_ymax,annotator_ymax]).flatten())
	dx   = xmax - xmin
	dy   = ymax - ymin

	if (dx <= 0) | (dy <= 0):
		iou = float(0)
	else:
		intersection = float(dx * dy)
		union        = float(gt_xmax - gt_xmin) * float(gt_ymax - gt_ymin) + \
		float(annotator_xmax - annotator_xmin) * float(annotator_ymax - annotator_ymin) - \
		intersection
		iou = intersection / union 
	return iou

def get_info(base_dir,pattern):
	#recursively look through base_dir and get all files whose names contain specified pattern
	name  = []
	sopuid    = []
	counter = 0

        for dirpath, dirs, files in os.walk(base_dir):
		current_json = []
                for i,filename in enumerate(files):
			if is_substring(np.array([pattern]),filename):
				current_json.append(i)
		current_json = np.array(current_json).flatten()
		if len(current_json) > 0:
			for i in current_json:
				name.append(dirpath + '/' + files[i])
				sopuid.append(files[i].replace(pattern,''))
				counter = counter + 1
		#if counter > max_files:
		#	break
	name     = np.array(name).flatten()
	sopuid   = np.array(sopuid).flatten()
	info     = PR(name,sopuid)
	return info

def get_coordinates(clock_angle,cmfn,clock,nipple,laterality,view,nipple_pointing,pixel_spacing):

	tmp = np.string_(clock_angle).split(':')
	if len(tmp) == 2:
		hour    = float(tmp[0])
		minutes = float(tmp[1])
	else:
		hour    = float(tmp[0])
		minutes = float(0)

	#angle in radians, relative to the 12:00 position (y-axis), clock-wise = positive
	angle = np.pi * (hour + minutes / 60) / 6

	#angle in radians, relative to the 3:00 position (x-axis), clock-wise = positive
	angle = angle - 0.5* np.pi

	#angle in radians, relative to the 3:00 position (x-axis), counter clock-wise = positive
	angle =  - angle


	#x = x coordinate on clock face
	#y = y coordinate in clock face 
	x = clock.r * np.cos(angle)
	y = clock.r * np.sin(angle)
	print 'clock_angle = ',clock_angle,', y_clock = ',y,', x_clock = ',x

	if nipple_pointing == 'r':
		if (view == 'mlo') | (view == 'ml'):
			slope    = (clock.y - y - nipple.y)/(clock.x - nipple.x)
			#factor 10 converts from cm to mm
			dx       = - (10 * cmfn / pixel_spacing)/np.sqrt(1 + slope * slope)
			dy       = slope * dx
			lesion_x = nipple.x + dx
			lesion_y = nipple.y + dy
		elif view == 'cc':
			if laterality == 'l':
				slope    = (clock.y - x - nipple.y)/(clock.x - nipple.x)
				#factor 10 converts from cm to mm
				dx       = - (10 * cmfn / pixel_spacing)/np.sqrt(1 + slope * slope)
				dy       = slope * dx
				lesion_x = nipple.x + dx
				lesion_y = nipple.y + dy
			else:
				slope    = (clock.y + x - nipple.y)/(clock.x - nipple.x)
				#factor 10 converts from cm to mm
				dx       = - (10 * cmfn / pixel_spacing)/np.sqrt(1 + slope * slope)
				dy       = slope * dx
				lesion_x = nipple.x + dx
				lesion_y = nipple.y + dy
		else:
			print 'invalid view: ',view
	elif nipple_pointing == 'l':
		if (view == 'mlo') | (view == 'ml'):
			slope    = (clock.y - y - nipple.y)/(clock.x - nipple.x)
			#factor 10 converts from cm to mm
			dx       = + (10 * cmfn / pixel_spacing)/np.sqrt(1 + slope * slope)
			dy       = slope * dx
			lesion_x = nipple.x + dx
			lesion_y = nipple.y + dy
		elif view == 'cc':
			if laterality == 'l':
				slope    = (clock.y - x - nipple.y)/(clock.x - nipple.x)
				#factor 10 converts from cm to mm
				dx       = + (10 * cmfn / pixel_spacing)/np.sqrt(1 + slope * slope)
				dy       = slope * dx
				lesion_x = nipple.x + dx
				lesion_y = nipple.y + dy
			else:
				slope    = (clock.y + x - nipple.y)/(clock.x - nipple.x)
				#factor 10 converts from cm to mm
				dx       = + (10 * cmfn / pixel_spacing)/np.sqrt(1 + slope * slope)
				dy       = slope * dx
				lesion_x = nipple.x + dx
				lesion_y = nipple.y + dy
		else:
			print 'invalid view: ',view

	lesion = POINT(lesion_x,lesion_y)
	return lesion

if __name__ == '__main__':

	parser = argparse.ArgumentParser(description = 'check accuracy of annotators (measured by ratio of overlap/union between \
	gt bounding box and annotated bounding box')

	parser.add_argument('-gt', dest = 'gt', help = 'directory containting gt PR files', default = './gt_pr')
	parser.add_argument('-a',  dest = 'annotator', help = "name of annotation outfit", default = 'annotator')
	parser.add_argument('-m',  dest = 'modality', help = "modality (tomo/2d)", default = 'tomo')
	parser.add_argument('-t',  dest = 'threshold', help = "threshold for intersection over union (iou)", default = 0.5)

	args = parser.parse_args()


	#set text properties
        mass_color  = (255,255,0)
        calcs_color = (255,0,255)
        font        = cv2.FONT_HERSHEY_SIMPLEX


	#pat,side,study_date_str,accession,sop,xmin,xmax,ymin,ymax,lesion_type,location,clock_angle,distance,zone
	#gt = pd.read_csv('../clock_angle_gt/prep-for-clock-angle/gt_n_clock_position.csv')
	#gt = pd.read_csv('../clock_angle_gt/prep-for-protractor/gt_n_clock_position.csv')
	gt = pd.read_csv('/Users/HVu/Documents/annotation_projects/clock_angle_gt/prep-for-clock-angle-other_datasets/other_datasets/gt_n_clock_position_M1_new_format.csv')

	for i in range(len(gt['sop'])):
		sopuid = gt.loc[i,'sop']

		#dcm_filename    = '../clock_angle_gt/prep-for-clock-angle/DCMs/' + gt.loc[i,'sop'] + '.dcm'
		#dcm_filename    = '../clock_angle_gt/prep-for-protractor/DCMs/' + gt.loc[i,'sop'] + '.dcm'
		dcm_filename    = '/Volumes/CureMetrix/hoanh/clock_angle_overlay_dcm/' + gt.loc[i,'sop'] + '.dcm'
		#png_filename    = './png/' + gt.loc[i,'sop'] + '.png'
		#png_filename    = './png/' + gt.loc[i,'sop'] + '_nomarks.png'
		png_filename    = './png_M1/' + gt.loc[i,'sop'] + '.png'
		#nipple_filename = '../clock_angle_gt/prep-for-clock-angle/JSON-Outputs/' + gt.loc[i,'sop'] + '_MassCalc.json'
		#nipple_filename = '../clock_angle_gt/prep-for-protractor/JSON-Outputs/' + gt.loc[i,'sop'] + '_MassCalc.json'
		nipple_filename = '../clock_angle_gt/prep-for-clock-angle-other_datasets/other_datasets/JSON-Outputs/' + gt.loc[i,'sop'] + '_MassCalc.json'
		#breast_contour_filename = '../clock_angle_gt/prep-for-clock-angle/BreastBoundary_Output/' + gt.loc[i,'sop'] + '.json'
		#breast_contour_filename = '../clock_angle_gt/prep-for-protractor/BreastBoundary_Output/' + gt.loc[i,'sop'] + '.json'
		breast_contour_filename = '../clock_angle_gt/prep-for-clock-angle-other_datasets/other_datasets/BreastBoundary_Output/' + gt.loc[i,'sop'] + '.json'
		if os.path.isfile(dcm_filename):
			#read DCM
			print 'reading: ',dcm_filename
                        dicom_object = pydicom.dcmread(dcm_filename)
                        im = np.array((float(255)/float(4096))* np.array(dicom_object.pixel_array[:,:]).astype('float')).astype('uint8')
			#get image size
                        image_size = im.shape
                        n_row      = image_size[0]
                        n_col      = image_size[1]
                        im_resized = cv2.resize(im, (n_col,n_row))
			#store image in im_overlay (3 channels image overlay array)
                        im_overlay = np.zeros((n_row,n_col,3)).astype('uint8')
                        im_overlay[:,:,0] = np.copy(im_resized)
                        im_overlay[:,:,1] = np.copy(im_resized)
                        im_overlay[:,:,2] = np.copy(im_resized)

			#"""

			
			#get nipple
			print 'nipple_filename = ',nipple_filename
			if os.path.isfile(nipple_filename):
				#get nipple's bounding box, centroid coordinates, and pertinent information regarding DCM image
				image_laterality,view_position,pixel_size,xmin,ymin,xmax,ymax,nipple = get_nipple(nipple_filename)
				#overlay nipple bounding box on image
                        	im_overlay = cv2.rectangle(im_overlay,(int(xmin),int(ymin)),(int(xmax),int(ymax)),(255,0,0),2)
				#overlay gt bounding box on image
                        	xmin = gt.loc[i,'xmin']
                        	ymin = gt.loc[i,'ymin']
                        	xmax = gt.loc[i,'xmax']
                        	ymax = gt.loc[i,'ymax']
				lesion_radius = np.amin([0.5*(xmax - xmin),0.5*(ymax - ymin)])
                        	im_overlay = cv2.rectangle(im_overlay,(xmin,ymin),(xmax,ymax),(0,0,255),2)

			#get breast outline
			if os.path.isfile(breast_contour_filename):
				breast_centroid,breast_contour,contour_x,contour_y = get_breast_outline(breast_contour_filename)
				#overlay breast contour on image
				im_overlay = cv2.polylines(im_overlay, np.array(breast_contour['contour']), True, (0,255,255), 3)

			#is nipple pointing left or right?
			nipple_pointing = get_nipple_pointing(nipple,breast_centroid)

			#get clock center and radius
			clock = get_clock(image_size,contour_x,contour_y,nipple,nipple_pointing)

			#overlay clock on chest wall
			contour_y_min = np.amin(contour_y)
			contour_y_max = np.amax(contour_y)
			im_overlay = cv2.line(im_overlay, (clock.x,contour_y_min), (clock.x,contour_y_max),  (0,255,0), 10)
				
			#get lesion's coordinates from clinical data(clock angle + cmfn)
			#clock_angle = gt.loc[i,'clock_angle_original']
			#cmfn        = float(gt.loc[i,'distance_original'])
			if (not pd.isnull(gt.loc[i,'clock_angle'])) & (not pd.isnull(gt.loc[i,'distance'])):
				if gt.loc[i,'distance'] != 'RTA':
					clock_angle = gt.loc[i,'clock_angle']
					cmfn        = float(gt.loc[i,'distance'])
					lesion      = get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
	
					#overlay lesion location on image
					im_overlay  = cv2.circle(im_overlay, (int(lesion.x),int(lesion.y)), int(lesion_radius),  (0,255,0), 10)


			#compute and draw protrator 
			clock_preset = np.array(['12:00','11:00','10:00','9:00','8:00','7:00','6:00','5:00','4:00','3:00','2:00','1:00']).flatten()
			cmfn_preset  = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]).flatten()

			#compute lines with preset clock angles
			curves = []
			label_position_x = []
			label_position_y = []
			for clock_angle in clock_preset:
				curve = []
				for cmfn in cmfn_preset:
					lesion      = get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
					pt          = []
					pt.append(int(round(lesion.x)))
					pt.append(int(round(lesion.y)))
					pt = np.array(pt).flatten()
					curve.append(pt)
				curves.append(curve)
                                #render mass score
                                x_min      = curves[-1][5][0]
                                y_min      = curves[-1][5][1]
				found = np.array(np.where((label_position_x == x_min) & (label_position_y == y_min))).flatten()
				if len(found) == 0:
                                	im_overlay = cv2.putText(im_overlay,clock_angle,(x_min,y_min-5), font, 1.75,mass_color,2,cv2.LINE_AA)
					label_position_x.append(x_min)
					label_position_y.append(y_min)
				else:
                                	x_min      = curves[-1][7][0]
                                	y_min      = curves[-1][7][1]
                                	im_overlay = cv2.putText(im_overlay,clock_angle,(x_min,y_min-5), font, 1.75,mass_color,2,cv2.LINE_AA)

			curves = np.array(curves)
			im_overlay = cv2.polylines(im_overlay, curves, True, (0,255,255), 1)

			#compute curves with preset cmfn
			curves = []
			for cmfn in cmfn_preset:
				curve = []
				for clock_angle in clock_preset:
					lesion      = get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
					pt          = []
					pt.append(int(round(lesion.x)))
					pt.append(int(round(lesion.y)))
					pt = np.array(pt).flatten()
					curve.append(pt)
				curves.append(curve)
			curves = np.array(curves)
			im_overlay = cv2.polylines(im_overlay, curves, True, (0,255,255), 1)

			#"""

                	#write to png
                	cv2.imwrite(png_filename,im_overlay)
  

		#print 'sopuid = ',gt.loc[i,'sop'],', laterality = ',gt.loc[i,'side'],', clock angle = ',gt.loc[i,'clock_angle'],', cmfn = ',\
		#gt.loc[i,'distance'],', nipple bounding box = [',gt.loc[i,'xmin'],', ',gt.loc[i,'ymin'],', ',gt.loc[i,'xmax'],', ',gt.loc[i,'ymax'],']',\
		#', nipple_pointing = ',nipple_pointing,', breast radius = ',clock.r,', image_laterality = ',image_laterality,\
		#', view position = ',view_position,', pixel_size = ',pixel_size,'nipple_x = ',nipple.x,', nipple_y = ',nipple.y,', lesion_x = ',lesion.x,\
		#', lesion_y = ',lesion.y
